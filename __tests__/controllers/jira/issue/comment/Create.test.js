import Jira from 'src/common/net/Jira'
import CommentCreateController from 'src/controllers/jira/issue/comment/Create'
import IssueShowController from 'src/controllers/jira/issue/Show'
import AddonError from '../../../../../src/common/AddonError'

jest.mock('src/common/analytics')
jest.mock('src/common/net/Jira')
jest.mock('src/controllers/jira/issue/Show')

const params = {
  instanceHost: 'dummy-domain.atlassian.net',
  issueKey: 'DEV-1337',
  newComment: 'lorem ipsum',
}

const createIssueComment = jest.fn()
Jira.mockImplementation(() => ({ createIssueComment }))

describe('Controller jira.comment.Create', () => {
  beforeEach(() => {
    Jira.mockClear()
  })

  it('Should call Jira with correct params', () => {
    new CommentCreateController().execute(params)

    expect(Jira).toBeCalledWith(params.instanceHost)
    expect(createIssueComment).toBeCalledWith(params.issueKey, {
      body: params.newComment,
    })
  })

  it('Should call issue.Show controller', () => {
    const execute = jest.fn()
    IssueShowController.mockImplementation(() => ({ execute }))

    new CommentCreateController().execute(params)

    expect(IssueShowController).toBeCalled()
    expect(execute).toBeCalledWith({ instanceHost: 'dummy-domain.atlassian.net', issueKey: 'DEV-1337' })
  })

  it('Should throw AddonError when no comment provided', () => {
    const execute = () => new CommentCreateController().execute({ ...params, newComment: '' })

    expect(execute).toThrow(AddonError)
    expect(execute).toThrowErrorMatchingSnapshot()
  })
})
