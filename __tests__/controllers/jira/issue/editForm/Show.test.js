import EditFormShowController from 'src/controllers/jira/issue/editForm/Show'
import EditFormView from 'src/views/jira/issue/EditForm'

jest.mock('src/common/analytics')

const params = {
  instanceHost: 'dummy-domain.atlassian.net',
  issue: {
    editmeta: { fields: { priority: true } },
  },
  priorities: Symbol('priorities'),
  editableFields: Symbol('editableFields'),
}

describe('Controller jira.Issue.editForm.Show', () => {
  it('Should return EditForm view', () => {
    expect(new EditFormShowController().execute(params)).toBeInstanceOf(EditFormView)
  })
})
