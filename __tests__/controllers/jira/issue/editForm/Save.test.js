import EditFormSaveController from 'src/controllers/jira/issue/editForm/Save'
import IssueShowController from 'src/controllers/jira/issue/Show'
import Jira from 'src/common/net/Jira'

jest.mock('src/common/analytics')
jest.mock('src/common/net/Jira')
jest.mock('src/views/jira/issue/Issue')
jest.mock('src/controllers/jira/issue/Show')

const params = {
  instanceHost: 'dummy-domain.atlassian.net',
  issueKey: 'DEV-123',
  summary: 'hello world',
  transition: '123',
  description: 'lorem ipsum',
  priority: '1337',
  assigneeEmail: 'Unassigned',
}

IssueShowController.mockImplementation(() => ({ execute: jest.fn() }))

const updateIssue = jest.fn().mockReturnThis()
const createIssueTransition = jest.fn().mockReturnThis()
const parallel = jest.fn().mockReturnThis()
const fetch = jest.fn()
Jira.mockImplementation(() => ({ updateIssue, createIssueTransition, parallel, fetch }))

describe('Controller jira.Issue.editForm.Save', () => {
  beforeEach(() => {
    Jira.mockClear()
    IssueShowController.mockClear()
  })

  it('Should call Jira with correct params', () => {
    new EditFormSaveController().execute(params)

    expect(Jira).toBeCalledWith(params.instanceHost)
    expect(updateIssue).toBeCalledWith('DEV-123', {
      fields: {
        description: 'lorem ipsum',
        priority: { id: '1337' },
        summary: 'hello world',
        assignee: { name: null },
      },
    })
    expect(createIssueTransition).toBeCalledWith('DEV-123', { transition: { id: '123' } })
  })

  it('Should call Issue Show controller with correct params', () => {
    const execute = jest.fn()
    IssueShowController.mockImplementation(() => ({ execute }))

    new EditFormSaveController().execute(params)
    expect(IssueShowController).toBeCalled()
    expect(execute).toBeCalledWith({ instanceHost: 'dummy-domain.atlassian.net', issueKey: 'DEV-123' })
  })
})
