import Jira from 'src/common/net/Jira'
import PreviewController from 'src/controllers/jira/issue/Preview'
import issue from '../../../helpers/mocks/responses/jira/getIssue.json'

jest.mock('src/common/net/Jira')

const params = {
  instanceHost: 'dummy-domain.atlassian.net',
  issueKey: 'DEV-1337',
}

const getIssue = jest.fn().mockReturnValue(issue)
Jira.mockImplementation(() => ({ getIssue }))

describe('Controller jira.Issue.Preview', () => {
  beforeEach(() => Jira.mockClear())

  it('Should call bitbucket', () => {
    new PreviewController().execute(params)
    expect(Jira).toBeCalledWith(params.instanceHost)
    expect(getIssue).toBeCalledWith(params.issueKey, {
      fields: 'project,summary,key,status,updated,issuetype',
    })
  })

  it('Should return preview object', () => {
    expect(new PreviewController().execute(params)).toEqual({
      key: 'DEV-11',
      source: 'Development',
      status: 'In Progress',
      title: 'Update task status by dragging and dropping from column to column >> Try dragging this task to "Done"',
      updatedAt: '2018-06-22T17:31:26.796+0300',
      statusCategory: 'indeterminate',
      iconUrl: 'https://main-jira-in-the-w0rld.atlassian.net/ex/jira/c51c0262-20cd-462d-b989-5a0aadcffec9/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
    })
  })
})
