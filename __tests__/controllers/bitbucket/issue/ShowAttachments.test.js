import IssueShowAttachmentsController from 'src/controllers/bitbucket/issue/ShowAttachments'
import Bitbucket from 'src/common/net/Bitbucket'
import IssueAttachmentView from 'src/views/bitbucket/issue/Attachments'

jest.mock('src/common/analytics')
jest.mock('src/common/net/Bitbucket')
jest.mock('src/views/bitbucket/issue/Attachments')

const issue = {
  id: 'id',
  repository: {
    name: 'name',
    owner: {
      username: 'username',
    },
  },
}

const attachments = Symbol('attachments')

const getIssue = jest.fn().mockReturnValue(issue)
const getIssueAttachments = jest.fn().mockReturnValue(attachments)

Bitbucket.mockImplementation(() => ({ getIssue, getIssueAttachments }))

describe('Controller bitbucket.issue.ShowAttachment', () => {
  beforeEach(() => jest.clearAllMocks())

  it('Should return Bitbucket issue  attachment view', () => {
    expect(new IssueShowAttachmentsController().execute({ issue }))
      .toBeInstanceOf(IssueAttachmentView)
  })

  it('Should call Bitbucket with correct params', () => {
    new IssueShowAttachmentsController().execute({ issue })

    expect(Bitbucket).toBeCalledWith(
      issue.repository.owner.username,
      issue.repository.name
    )

    expect(getIssueAttachments).toBeCalledWith(issue.id)
  })

  it('Should call Bitbucket issue attachment view with correct params', () => {
    new IssueShowAttachmentsController().execute({ issue })
    expect(IssueAttachmentView).toBeCalledWith({ attachments, issue })
  })
})
