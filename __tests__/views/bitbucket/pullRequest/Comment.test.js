import { Card } from 'gas-mock-service'
import CommentView from 'src/views/bitbucket/pullRequest/Comment'
import pullRequest from '../../../helpers/mocks/responses/bitbucket/getPullRequest.json'
import comments from '../../../helpers/mocks/responses/bitbucket/getPullRequestCommentsList.json'

describe('View Pull Request Comment', () => {
  it('Runs the render method and checks the data of used services', () => {
    const card = new CommentView({
      pullRequest,
      comment: comments.values[0],
    }).render()

    expect(card).toBeInstanceOf(Card)
    expect(card.printJson()).toMatchSnapshot()
  })
})
