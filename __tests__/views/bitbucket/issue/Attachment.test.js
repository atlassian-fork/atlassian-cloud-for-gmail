
import { Card } from 'gas-mock-service'
import { cloneDeep } from 'lodash'
import IssueAttachmentView from 'src/views/bitbucket/issue/Attachments'
import issue from '../../../helpers/mocks/responses/bitbucket/getIssue.json'
import attachments from '../../../helpers/mocks/responses/bitbucket/getIssueAttachments.json'
import repository from '../../../helpers/mocks/responses/bitbucket/getRepository.json'

describe('View Issue Attachment', () => {
  it('Runs the render method and checks the data of used services', () => {
    const card = new IssueAttachmentView({ issue, attachments, repository }).render()

    expect(card).toBeInstanceOf(Card)
    expect(card.printJson()).toMatchSnapshot()
  })

  it('Runs the render method and checks the data of used services when repository is not private', () => {
    const clonedRepository = cloneDeep(issue)
    clonedRepository.is_private = 'false'

    const card = new IssueAttachmentView({ issue, attachments, repository }).render()

    expect(card).toBeInstanceOf(Card)
    expect(card.printJson()).toMatchSnapshot()
  })
})
