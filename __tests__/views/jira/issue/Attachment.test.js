import { Card } from 'gas-mock-service'
import IssueAttachmentView from 'src/views/jira/issue/Attachments'

const issue = {
  key: 'DEV-1337',
  fields: {
    summary: 'dummy-text',
    attachment: [],
  },
  renderedFields: {
    description: 'dummy-description-text',
  },
}

const instanceHost = 'dummy-domain.atlassian.net'

describe('View Issue Attachment', () => {
  it('Runs the render method and checks the data of used services', () => {
    const card = new IssueAttachmentView({ instanceHost, issue }).render()

    expect(card).toBeInstanceOf(Card)
    expect(card.printJson()).toMatchSnapshot()
  })
})
