import { Card } from 'gas-mock-service'
import IssueDescriptionView from 'src/views/jira/issue/Description'

const issue = {
  key: 'DEV-1337',
  fields: {
    summary: 'dummy-text',
  },
  renderedFields: {
    description: 'dummy-description-text',
  },
}
const instanceHost = 'dummy-domain.atlassian.net'

describe('View Issue Description', () => {
  it('Runs the render method and checks the data of used services', () => {
    const card = new IssueDescriptionView({
      instanceHost,
      issue,
    }).render()

    expect(card).toBeInstanceOf(Card)
    expect(card.printJson()).toMatchSnapshot()
  })
})
