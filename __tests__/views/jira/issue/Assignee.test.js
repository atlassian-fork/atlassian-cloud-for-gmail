import AssigneeView from 'src/views/jira/issue/Assignee'
import issue from '../../../helpers/mocks/responses/jira/getIssueWithTransitions.json'

const params = {
  instanceHost: 'main-jira-in-the-w0rld.atlassian.net',
  issue,
}

describe('View Issue/Assignee', () => {
  it('Should check that the issue Assignee card data is correct', () => {
    const card = new AssigneeView(params).render()

    expect(card.printJson()).toMatchSnapshot()
  })
})
