import { Card } from 'gas-mock-service'
import CommentView from 'src/views/jira/issue/Comment'
import issue from '../../../helpers/mocks/responses/jira/getIssue.json'
import issueComments from '../../../helpers/mocks/responses/jira/getComments.json'

describe('View Issue Comment', () => {
  it('Runs the render method and checks the data of used services', () => {
    const card = new CommentView({
      instanceHost: 'dummy-domain.atlassian.net',
      issue,
      comment: issueComments.comments[0],
    }).render()

    expect(card).toBeInstanceOf(Card)
    expect(card.printJson()).toMatchSnapshot()
  })
})
