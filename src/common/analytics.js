import querystring from 'querystring'
import shajs from 'sha.js'
import { get } from 'lodash'
import { scriptProperties } from './properties'
import { pickBy } from './utils'

class GoogleAnalytics {
  init (userEmail) {
    this.cid = shajs('sha256').update(userEmail).digest('hex')
    this.tid = get(scriptProperties.get('googleAnalytics'), 'trackingId')
    this.events = []
  }

  event ({ category, action, label, value }) {
    this._enqueueEvent({
      t: 'event',
      ec: category,
      ea: action,
      el: label,
      ev: value,
    })
  }

  pageView (viewName) {
    this._enqueueEvent({
      t: 'pageview',
      dp: viewName,
    })
  }

  flush () {
    if (!this.tid || !this.events.length) return

    const defaults = {
      v: '1',
      an: 'Gmail Addon',
      tid: this.tid,
      cid: this.cid,
    }

    const payload = this.events
      .map(e => querystring.stringify({
        ...defaults,
        ...pickBy(e, Boolean), // GA does not accept empty/null/undefined in query params
      }))
      .join('\n')

    UrlFetchApp.fetch('https://www.google-analytics.com/batch', {
      payload,
      method: 'POST',
      muteHttpExceptions: true,
    })
  }

  _enqueueEvent (event) {
    this.events.push(event)
    // google analytics allows max 20 events per single batch request
    if (this.events.length === 20) {
      this.flush()
    }
  }
}

export default new GoogleAnalytics()
