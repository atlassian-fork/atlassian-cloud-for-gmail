import { get, set } from 'lodash'
import HttpStatus from 'http-status-codes'
import { formatUrl } from '../utils'
import { scriptProperties } from '../properties'
import AddonError from '../AddonError'
import HttpClient from './HttpClient'

export default class Bitbucket {
  constructor (owner, repo) {
    this._owner = owner
    this._repo = repo
    this._httpClient = new HttpClient()
  }

  static oauthService () {
    const credentials = scriptProperties.get('bitbucketCredentials')

    return OAuth2.createService('bitbucket')
      .setAuthorizationBaseUrl('https://bitbucket.org/site/oauth2/authorize')
      .setTokenUrl('https://bitbucket.org/site/oauth2/access_token')
      .setClientId(credentials.clientId)
      .setClientSecret(credentials.clientSecret)
      .setCallbackFunction('bitbucketOAuthCallback')
      .setPropertyStore(PropertiesService.getUserProperties())
      .setCache(CacheService.getUserCache())
  }

  parallel () {
    this._httpClient.setParallel(true)
    return this
  }

  getCurrentUser (query) {
    return this._enqueue({
      pathname: '/2.0/user',
      query,
    })
  }

  getCurrentUserEmails (query) {
    return this._enqueue({
      pathname: '/2.0/user/emails',
      query,
    })
  }

  getUser (name) {
    return this._enqueue(
      { pathname: `/2.0/users/${name}` },
      { headers: { Authorization: false } }
    )
  }

  getTeam (name) {
    return this._enqueue(
      { pathname: `/2.0/teams/${name}` },
      { headers: { Authorization: false } }
    )
  }

  getIssue (issueId, query) {
    return this._enqueue({
      pathname: `/2.0/repositories/${this._owner}/${this._repo}/issues/${issueId}`,
      query,
    })
  }

  updateIssue (issueId, body) {
    return this._enqueue(
      { pathname: `/2.0/repositories/${this._owner}/${this._repo}/issues/${issueId}` },
      { method: 'PUT', body },
    )
  }

  getIssueComments (issueId, query) {
    return this._enqueue({
      pathname: `/2.0/repositories/${this._owner}/${this._repo}/issues/${issueId}/comments`,
      query,
    })
  }

  createIssueComment (issueId, rawContent) {
    return this._enqueue(
      { pathname: `/2.0/repositories/${this._owner}/${this._repo}/issues/${issueId}/comments` },
      {
        method: 'POST',
        body: { content: { raw: rawContent } },
      }
    )
  }

  getIssueAttachments (issueId, query) {
    return this._enqueue({
      pathname: `/2.0/repositories/${this._owner}/${this._repo}/issues/${issueId}/attachments`,
      query,
    })
  }

  getIssueWatchStatus (issueId) {
    return this._enqueue({ pathname: `/2.0/repositories/${this._owner}/${this._repo}/issues/${issueId}/watch` })
  }

  watchIssue (issueId, watch) {
    const method = watch ? 'PUT' : 'DELETE'
    this._enqueue(
      { pathname: `/2.0/repositories/${this._owner}/${this._repo}/issues/${issueId}/watch` },
      {
        method,
        body: {}, // API requires a call to be an 'empty' PUT
      }
    )
  }

  getPullRequest (pullRequestId, query) {
    return this._enqueue({
      pathname: `/2.0/repositories/${this._owner}/${this._repo}/pullrequests/${pullRequestId}`,
      query,
    })
  }

  getPullRequestStatusList (pullRequestId) {
    return this._enqueue({ pathname: `/2.0/repositories/${this._owner}/${this._repo}/pullrequests/${pullRequestId}/statuses` })
  }

  getPullRequestComments (pullRequestId, query) {
    return this._enqueue({
      pathname: `/2.0/repositories/${this._owner}/${this._repo}/pullrequests/${pullRequestId}/comments`,
      query,
    })
  }

  createPullRequestComment (pullRequestId, { content, parentCommentId }) {
    const body = { content: { raw: content } }

    if (parentCommentId) set(body, 'parent.id', parseInt(parentCommentId, 10))

    return this._enqueue(
      { pathname: `/2.0/repositories/${this._owner}/${this._repo}/pullrequests/${pullRequestId}/comments` },
      { method: 'POST', body }
    )
  }

  mergePullRequest (pullRequestId, body = {}) {
    return this._enqueue(
      { pathname: `/2.0/repositories/${this._owner}/${this._repo}/pullrequests/${pullRequestId}/merge` },
      { method: 'POST', body }
    )
  }

  getPipeline (pipelineId, query) {
    return this._enqueue({
      pathname: `/2.0/repositories/${this._owner}/${this._repo}/pipelines/${pipelineId}`,
      query,
    })
  }

  rerunPipeline (commitId, branchName) {
    const body = {
      target: {
        commit: {
          hash: commitId,
          type: 'commit',
        },
        ref_type: 'branch',
        type: 'pipeline_ref_target',
        ref_name: branchName,
      },
    }

    return this._enqueue(
      { pathname: `/2.0/repositories/${this._owner}/${this._repo}/pipelines/` },
      { method: 'POST', body }
    )
  }

  fetch () {
    try {
      return this._httpClient.execute()
    } catch (error) {
      if (!error.response) throw error

      const { statusCode, body } = error.response
      const message = body.message || get(body, 'error.message') || body.error_description || ''

      if (message === 'Bad credentials' ||
        message === 'Invalid refresh_token' ||
        message.includes('Access token expired')
      ) {
        throw new AddonError('Authorization required', 'UNAUTHORIZED', 'BITBUCKET')
      }

      if (statusCode === HttpStatus.FORBIDDEN) {
        throw new AddonError('No access to resource', 'FORBIDDEN', 'BITBUCKET')
      }

      if (statusCode === HttpStatus.NOT_FOUND) {
        throw new AddonError('Object not found in Bitbucket', 'NOT_FOUND', 'BITBUCKET')
      }

      throw error
    }
  }

  _enqueue ({ pathname, query = {} }, { method, body, headers = {} } = {}) {
    const oauthService = Bitbucket.oauthService()

    if (!oauthService.hasAccess()) {
      throw new AddonError('Authorization required', 'UNAUTHORIZED', 'BITBUCKET')
    }

    const url = formatUrl({
      host: 'https://api.bitbucket.org',
      pathname,
      query,
    })

    if (headers.Authorization === undefined) {
      headers.Authorization = `Bearer ${oauthService.getAccessToken()}`
    }

    this._httpClient.enqueueRequest(url, { method, body, headers })

    if (this._httpClient.isParallel()) {
      return this
    }

    return this.fetch()
  }
}
