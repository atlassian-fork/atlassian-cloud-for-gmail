import 'core-js'
import { get } from 'lodash'
import controllers from './controllers'

export function getContextualAddOn (event) {
  return new controllers.addon.Show({ event }).run()
}

export function getComposeAddOn (event) {
  return [new controllers.addon.ShowCompose({ event }).run({})]
}

export function getSettings (event) {
  return new controllers.addon.ShowSettings({ event }).run()
}

export function getFeedback () {
  return new controllers.addon.feedback.Show().run()
}

export function bitbucketOAuthCallback (response) {
  return new controllers.OAuthCallback().run({ response, product: 'bitbucket' })
}

export function jiraOAuthCallback (response) {
  return new controllers.OAuthCallback().run({ response, product: 'jira' })
}

export function dispatchAction (event) {
  const { formInput, parameters } = event
  const { actionName, args } = parameters

  const Action = get(controllers, actionName)
  if (!Action) {
    throw new Error(`Action ${actionName} not found`)
  }

  return new Action({ event }).run({
    ...JSON.parse(args),
    ...formInput,
  })
}
