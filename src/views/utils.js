import moment from 'moment-timezone'
import urlParse from 'url-parse'
import { upperFirst } from 'lodash'
import {
  truncate as truncateHtml,
  convert as convertHtml
} from '@atlassian-partner-engineering/atlassian-html-to-gmail'

export function createAction (actionName, args = {}) {
  return CardService.newAction()
    .setFunctionName('dispatchAction')
    .setParameters({
      actionName,
      args: JSON.stringify(args),
    })
}

export function createExternalLink (url) {
  return CardService.newOpenLink()
    .setUrl(url)
    .setOpenAs(CardService.OpenAs.FULL_SIZE)
}

export function createKeyValueWidget ({
  label, bottomLabel, content, multiline, icon, action, openLink,
}) {
  const widget = CardService.newKeyValue()
  if (content) widget.setContent(content)
  if (multiline) widget.setMultiline(multiline)
  if (label) widget.setTopLabel(label)
  if (bottomLabel) widget.setBottomLabel(bottomLabel)
  if (action) widget.setOnClickAction(action)
  if (openLink) widget.setOpenLink(openLink)
  if (icon) widget.setIconUrl(icon)
  return widget
}

export function createActionButtonWidget (text, actionName, actionParams) {
  return CardService.newTextButton()
    .setText(text)
    .setOnClickAction(createAction(actionName, actionParams))
}

export function createLinkButtonWidget (text, url) {
  return CardService.newTextButton()
    .setText(text)
    .setOpenLink(CardService.newOpenLink().setUrl(url))
}

export function formatDateTime (value) {
  return value ? moment(value).fromNow() : '---'
}

export function formatDateWithTimezone ({ date, timeZone, format }) {
  return moment(date).tz(timeZone).format(format)
}

export function formatDuration (seconds) {
  return moment.duration(seconds, 'seconds').humanize()
}

export function capitalizeString (string) {
  return upperFirst(string.toLowerCase())
}

export function colorString (string = '-', hexColor = '#000000') {
  return `<font color="${hexColor}">${string}</font>`
}

export function pluralizeString (num, singularNoun) {
  return `${num} ${singularNoun}${(num === 1 ? '' : 's')}`
}

export function formatJiraIconUrl (iconUrl, currentHost, unsafe = false) {
  const parsedUrl = urlParse(iconUrl, true)
  // For icons hosted by Jira, convert them to PNG and increase them in size
  // We can't make assumptions about icons hosted elsewhere, so don't mess with the URL
  if (unsafe || parsedUrl.host === currentHost) {
    parsedUrl.set('query', {
      ...parsedUrl.query,
      format: 'png',
      size: 'xxlarge',
    })
  }

  return parsedUrl.toString()
}

export function formatHtml ({ html = '', defaultValue = '', truncate = true, limit = 100 }) {
  const convertedHtml = convertHtml(html.trim() || defaultValue)

  return truncate
    ? truncateHtml(convertedHtml, limit)
    : convertedHtml
}
