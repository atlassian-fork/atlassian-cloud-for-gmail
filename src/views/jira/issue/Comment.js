import widgets from '../widgets'
import JiraView from '../Jira'

export default class extends JiraView {
  get url () {
    return `${super.url}?page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel#activitymodule`
  }

  get sections () {
    return CardService.newCardSection()
      .setHeader('<b>Comment</b>')
      .addWidget(widgets.comment({
        comment: this.props.comment,
        truncate: false,
      }))
  }

  setProductLinkButtonSection () {
    return super.setProductLinkButtonSection(`View Comment In ${this.product}`)
  }
}
