import { get } from 'lodash'
import JiraView from '../Jira'
import widgets from '../widgets'
import Icons from '../../Icons'
import {
  createKeyValueWidget,
  createAction,
  createActionButtonWidget,
  formatJiraIconUrl,
  colorString,
  capitalizeString,
  pluralizeString,
  createExternalLink,
  formatHtml
} from '../../utils'
import Colors, { pickStatusColor } from '../../Colors'

const COLLAPSIBLE_NUM = 8

export default class extends JiraView {
  get sections () {
    return [
      this._getDetailSection(),
      this._getActionsSection(),
      this._getCommentsSection(),
    ]
  }

  _getDetailSection () {
    const { instanceHost, issue } = this.props
    const {
      project,
      issuetype,
      status,
      assignee,
      priority,
      reporter,
      attachment,
      labels,
      components,
    } = issue.fields

    const cardSection = CardService.newCardSection()
      .setCollapsible(true)
      .setNumUncollapsibleWidgets(COLLAPSIBLE_NUM)
      .addWidget(createKeyValueWidget({
        content: project.name,
        bottomLabel: this._getProjectType(),
      }))
      .addWidget(createKeyValueWidget({
        content: issuetype.name,
        icon: this._getIssueTypeIcon(issuetype.name),
      }))

    if (priority) {
      cardSection
        .addWidget(createKeyValueWidget({
          content: priority.name,
          icon: this._getPriorityIcon(priority.name),
        }))
    }

    cardSection
      .addWidget(createKeyValueWidget({
        label: 'Status',
        content: colorString(capitalizeString(status.name), pickStatusColor(
          status.statusCategory.key,
          'jira',
          'issue'
        )),
      }))
      .addWidget(widgets.user({
        user: assignee,
        label: 'Assignee',
        instanceHost,
      }).setButton(this._getAssignButton()))
      .addWidget(createKeyValueWidget({
        label: 'Description',
        content: formatHtml({
          html: issue.renderedFields.description,
          defaultValue: 'No description.',
        }),
        multiline: true,
        action: createAction('jira.issue.ShowDescription', { instanceHost, issueKey: issue.key }),
      }))

    if (attachment) {
      const widgetParams = {
        content: pluralizeString(attachment.length, 'attachment'),
        icon: Icons.attachment,
      }

      if (attachment.length) { // make it clickable if there are attachments
        widgetParams.action = createAction(
          'jira.issue.ShowAttachments',
          { instanceHost, issueKey: issue.key }
        )
      }

      cardSection.addWidget(createKeyValueWidget(widgetParams))
    }

    cardSection
      .addWidget(widgets.user({
        user: reporter,
        label: 'Reporter',
        instanceHost,
      }))

    if (labels) {
      cardSection.addWidget(createKeyValueWidget({
        label: 'Labels',
        content: this._concatList(labels),
      }))
    }

    cardSection
      .addWidget(createKeyValueWidget({
        label: 'Time estimate',
        content: this._getTimeEstimate(),
      }))
      .addWidget(createKeyValueWidget({
        label: 'Time tracking',
        content: this._getTimeSpent(),
      }))

    if (components) {
      cardSection.addWidget(createKeyValueWidget({
        label: 'Components',
        content: this._concatList(components.map(component => component.name)),
      }))
    }

    return cardSection
  }

  _getActionsSection () {
    const { instanceHost, issue, currentUser, priorities, editableFields } = this.props
    const isWatching = get(issue, 'fields.watches.isWatching')

    const buttonSet = CardService.newButtonSet()
    if (editableFields.length) {
      buttonSet.addButton(createActionButtonWidget(
        'Edit Issue',
        'jira.issue.editForm.Show',
        { instanceHost, issue, priorities, editableFields }
      ))
    }

    if (isWatching !== undefined) {
      const buttonText = isWatching ? 'Stop watching' : 'Watch'

      buttonSet
        .addButton(createActionButtonWidget(
          buttonText,
          'jira.issue.Watch',
          {
            instanceHost,
            issueKey: issue.key,
            watch: !isWatching,
            accountId: currentUser.account_id,
          }
        ))
    }

    return CardService.newCardSection().addWidget(buttonSet)
  }

  _getCommentsSection () {
    const { instanceHost, issue, issueComments } = this.props
    const { maxResults, total, comments } = issueComments

    const newCommentInput = CardService.newTextInput()
      .setTitle('Add a comment...')
      .setFieldName('newComment')
      .setMultiline(true)

    const cardSection = CardService.newCardSection()

    if (total > 3) { // leaves exactly 3 top comments
      cardSection
        .setCollapsible(true)
        .setNumUncollapsibleWidgets(5) // cuz input + button + 3 comments
    }

    cardSection
      .addWidget(newCommentInput)
      .addWidget(createActionButtonWidget('Save Comment', 'jira.issue.comment.Create', {
        issueKey: issue.key,
        instanceHost,
      }))

    comments.forEach((comment) => {
      const action = createAction('jira.issue.comment.Show', { comment, issue, instanceHost })
      cardSection.addWidget(widgets.comment({ comment, action }))
    })

    if (total > maxResults) {
      cardSection
        .addWidget(createKeyValueWidget({
          content: colorString(`Show <b>${total}</b> comments in Jira`, Colors.blue),
          openLink: createExternalLink(`${this.url}#comment-tabpanel`),
        }))
    }

    return cardSection
  }

  _getIssueTypeIcon () {
    const { instanceHost, issue } = this.props

    return formatJiraIconUrl(issue.fields.issuetype.iconUrl, instanceHost)
  }

  _getPriorityIcon () {
    const { instanceHost, issue } = this.props

    return formatJiraIconUrl(issue.fields.priority.iconUrl, instanceHost)
  }

  _getTimeEstimate () {
    const { timetracking } = this.props.issue.fields

    return get(timetracking, 'originalEstimate') || 'None'
  }

  _getTimeSpent () {
    const { timetracking } = this.props.issue.fields
    const timeSpent = get(timetracking, 'timeSpent')

    return timeSpent ? `${timeSpent} logged` : 'None'
  }

  _getProjectType () {
    return {
      business: 'Business project',
      service_desk: 'Service desk project',
      software: 'Software project',
    }[this.props.issue.fields.project.projectTypeKey] || 'Project'
  }

  _concatList (items) {
    return (items && items.length) ? items.join(', ') : 'None'
  }

  _getAssignButton () {
    const { issue, instanceHost } = this.props
    return createActionButtonWidget('Assign', 'jira.issue.assignee.Show', {
      issue, instanceHost,
    })
  }
}
