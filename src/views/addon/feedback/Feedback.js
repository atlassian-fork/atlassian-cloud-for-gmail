import View from '../../View'
import Icons from '../../Icons'
import { createActionButtonWidget } from '../../utils'

export default class extends View {
  get header () {
    return CardService.newCardHeader()
      .setTitle('Give feedback')
      .setImageUrl(Icons.feedback)
  }

  get sections () {
    const paragraph = CardService.newTextParagraph()
      .setText('Tell us what you think about the add-on or let us know if there are any crucial features missing.')

    const rating = CardService.newSelectionInput()
      .setType(CardService.SelectionInputType.RADIO_BUTTON)
      .setTitle('What do you think of the add-on?')
      .setFieldName('rating')
      .addItem('Awesome', 5, true)
      .addItem('Good', 4, false)
      .addItem('Okay', 3, false)
      .addItem('Bad', 2, false)
      .addItem('Terrible', 1, false)

    const rateReason = CardService.newTextInput()
      .setTitle('Why did you rate it this way?')
      .setFieldName('ratingReason')
      .setMultiline(true)

    const missingFeatures = CardService.newTextInput()
      .setTitle('Are there any features missing?')
      .setFieldName('missingFeatures')
      .setMultiline(true)

    const actions = CardService.newButtonSet()
      .addButton(createActionButtonWidget('Submit', 'addon.feedback.Submit', {
        user: this.props.currentUserId || '-',
      }))
      .addButton(createActionButtonWidget('Cancel', 'addon.NavigateBack'))

    const section = CardService.newCardSection()
      .addWidget(paragraph)
      .addWidget(rating)
      .addWidget(rateReason)
      .addWidget(missingFeatures)

    if (this.props.currentUserId) {
      section.addWidget(this._getAllowContactBackWidget())
    }

    section.addWidget(actions)

    return section
  }

  _getAllowContactBackWidget () {
    return CardService.newSelectionInput()
      .setType(CardService.SelectionInputType.CHECK_BOX)
      .setFieldName('allowContactBack')
      .addItem('Atlassian can contact me about my feedback', 'yes', true)
  }
}
