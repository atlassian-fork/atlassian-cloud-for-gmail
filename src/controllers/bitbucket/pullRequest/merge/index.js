import Show from './Show'
import Merge from './Merge'

export default {
  Show,
  Merge,
}
