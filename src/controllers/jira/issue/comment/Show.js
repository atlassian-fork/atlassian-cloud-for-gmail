import ActionPushController from '../../../ActionPushController'
import IssueCommentView from '../../../../views/jira/issue/Comment'

export default class extends ActionPushController {
  execute ({ comment, issue, instanceHost }) {
    return new IssueCommentView({ comment, issue, instanceHost })
  }
}
