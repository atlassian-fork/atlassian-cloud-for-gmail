import ActionPushController from '../../../ActionPushController'
import AssigneeView from '../../../../views/jira/issue/Assignee'

export default class extends ActionPushController {
  execute ({ issue, instanceHost }) {
    return new AssigneeView({ issue, instanceHost })
  }
}
