import Controller from './Controller'
import { getNavigationActionResponse } from './responseWrappers'
import AlertView from '../views/addon/Alert'

/**
 * Pops current card from the history and updates
 * the previous one with card returned from execute method
 */
export default class extends Controller {
  navigateTo (view) {
    // AlerView should be pushed to the history so users
    // can click on 'back' button when error fires
    const forward = view instanceof AlertView

    return getNavigationActionResponse(view.render(), forward)
  }
}
