import ActionController from '../../ActionController'
import FeedbackSuccessView from '../../../views/addon/feedback/Success'
import Gas from '../../../common/net/Gas'
import analytics from '../../../common/analytics'

export default class extends ActionController {
  execute ({
    user,
    allowContactBack,
    rating,
    missingFeatures,
    ratingReason,
  }) {
    const payload = {
      name: 'feedback',
      server: '-',
      product: 'atlassian-gmail-integration',
      user,
      serverTime: Date.now(),
      properties: {
        allowContactBack,
        rating,
        missingFeatures,
        ratingReason,
      },
    }

    new Gas().send(payload)

    analytics.event({
      category: 'Feedback',
      action: 'Submit',
    })

    return new FeedbackSuccessView()
  }
}
