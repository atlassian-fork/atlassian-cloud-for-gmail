import { get, uniqBy } from 'lodash'
import { getUrls } from '../../common/utils'
import { userProperties } from '../../common/properties'
import analytics from '../../common/analytics'
import AddonError from '../../common/AddonError'
import Gmail from '../../common/net/Gmail'
import Bitbucket from '../../common/net/Bitbucket'
import Jira from '../../common/net/Jira'
import ActionPushController from '../ActionPushController'
import controllers from '../'
import PreviewsView from '../../views/addon/Previews'
import BlankView from '../../views/addon/Blank'
import BitbucketOnboardingView from '../../views/addon/onboarding/Bitbucket'
import JiraOnboardingView from '../../views/addon/onboarding/Jira'
import jiraProjects from '../../services/jiraProjects'
import extractors from './extractors'

export default class extends ActionPushController {
  execute () {
    const { messageId, accessToken } = this.messageMetadata
    const message = new Gmail(accessToken).getMessage(messageId)
    const entities = this._extractEntities(message.getBody())

    const onboardingView = this._getOnboardingView()

    if (onboardingView) {
      return onboardingView
    }

    if (entities.length === 0) {
      analytics.pageView('Blank View')
      // blank view doesn't have any useful information for the user
      // so let's treat blank view as a place for background stuff
      // and sync jira projects while it's opening
      jiraProjects.sync()
      return new BlankView()
    }

    if (entities.length === 1) {
      analytics.pageView('Single Entity View')
      return this._getCompleteEntityView(entities[0])
    }

    analytics.pageView('Multiple Entities View')
    return this._getMultipleEntitiesView(entities)
  }

  _getOnboardingView () {
    const onboardingState = userProperties.get('onboardingState')

    if (!onboardingState) {
      analytics.pageView('Jira Onboarding')
      return new JiraOnboardingView({
        authUrl: Jira.oauthService().getAuthorizationUrl(),
      })
    }

    if (onboardingState === 'bitbucket') {
      analytics.pageView('Bitbucket Onboarding')
      return new BitbucketOnboardingView({
        authUrl: Bitbucket.oauthService().getAuthorizationUrl(),
      })
    }
  }

  _getCompleteEntityView (entity) {
    const ShowController = get(controllers, `${entity.product}.${entity.name}.Show`)

    if (!ShowController) {
      throw new Error(`Show controller for ${entity.name} not found`)
    }

    try {
      return new ShowController(this).execute(entity.args)
    } catch (error) {
      if (error instanceof AddonError) {
        if (error.code === 'UNAUTHORIZED') {
          // render authorization prompt for single entity
          return this._getPreviewsView({ forbiddenEntities: [entity] })
        }
        // render empty initial card
        return this._getPreviewsView()
      }

      throw error
    }
  }

  _getMultipleEntitiesView (entities) {
    const forbiddenEntities = [] // entities user has no access to

    const previews = entities
      .map((entity) => {
        const PreviewController = get(controllers, `${entity.product}.${entity.name}.Preview`)

        if (!PreviewController) {
          throw new Error(`Preview controller for ${entity.name} not found`)
        }

        try {
          const preview = new PreviewController(this).execute(entity.args)
          return {
            ...preview,
            entity,
          }
        } catch (error) {
          if (error instanceof AddonError) {
            if (error.code === 'UNAUTHORIZED') {
              forbiddenEntities.push(entity)
            }

            return null
          }

          throw error
        }
      })
      .filter(Boolean)

    return this._getPreviewsView({ previews, forbiddenEntities })
  }

  _getPreviewsView ({ previews = [], forbiddenEntities = [] } = {}) {
    return new PreviewsView({
      previews,
      forbiddenEntities,
      jiraAuthUrl: Jira.oauthService().getAuthorizationUrl(),
      bitbucketAuthUrl: Bitbucket.oauthService().getAuthorizationUrl(),
    })
  }

  _extractEntities (messageBody) {
    messageBody = messageBody
      .replace(/&amp;/g, '&') // decode HTML-encoded '&' which could appear in query string
      .replace(/<wbr>/g, '') // clean the word breaks sometimes inserted in text by mail clients

    const entities = [
      ...this._extractUrlEntities(messageBody),
      ...this._extractIssueKeysEntities(messageBody),
    ]

    return uniqBy(entities, entity => [
      entity.product,
      entity.name,
      ...Object.values(entity.args),
    ].join())
  }

  _extractUrlEntities (content) {
    const urls = getUrls(content)

    return urls
      .map((url) => {
        const extractor = extractors.find(e => e.regexp.test(url))
        if (!extractor) return null

        const { product, name, regexp, mapGroups } = extractor
        const args = mapGroups(url.match(regexp))

        return { args, product, name }
      })
      .filter(Boolean)
  }

  _extractIssueKeysEntities (messageBody) {
    const regexp = /(^|\s|>)([A-Z0-9]{1,10}-\d+)\b/g
    const issueKeys = []

    let match
    // eslint-disable-next-line no-cond-assign
    while (match = regexp.exec(messageBody)) {
      issueKeys.push(match[2])
    }

    return issueKeys.flatMap((issueKey) => {
      const projectKey = issueKey.split('-')[0]
      // NOTE: multiple jira sites can have the same project key
      const instances = jiraProjects.findInstancesByProject(projectKey)
      return instances.map(instanceHost => ({
        product: 'jira',
        name: 'issue',
        args: { instanceHost, issueKey },
      }))
    })
  }
}
